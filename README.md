Czech translation for [Automatic Animation](https://foundryvtt.com/packages/autoanimations) module. Works with [Czech compendium](foundryvtt-dnd5e-compendium-cz)

### Instalace

V hlavním menu kliknout na 'Add-On Modules' a poté kliknout 'Install Module' a dole do 'Manifest URL' vložit adresu níže.

https://gitlab.com/lehovec/automated-jb2a-animations-cz-lang/-/raw/master/module.json

Nezapomeň aktivovat modul!
